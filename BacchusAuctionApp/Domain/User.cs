﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class User
    {
        [Required] public string Id { get; set; }

        [Required] public string FirstName { get; set; }

        [Required] public string LastName { get; set; }

        public DateTime RegistrationDateTime { get; set; } = DateTime.Now;
        public List <Bidding> Biddings { get; set; } = new List<Bidding>();
        public User() { }
        public User(string fName, string lName, DateTime regDate)
        {
            FirstName = fName;
            LastName = lName;
            RegistrationDateTime = regDate;
            Id = FirstName + LastName + RegistrationDateTime;
        }
    }
}
