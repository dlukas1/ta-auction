﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Domain
{
    public class Auction
    {
        //ID
        [Key]
        [JsonProperty(PropertyName = "productId")]
        public Guid ProductId { get; set; }

        //NAME
        [JsonProperty(PropertyName = "productName")]
        public string ProductName { get; set; }

        //DESCRIPTION
        [JsonProperty(PropertyName = "productDescription")]
        public string ProductDescription { get; set; }


        //CATEGORY
        [JsonProperty(PropertyName = "productCategory")]
        public string ProductCategory { get; set; }

        //BIDDING END DATE
        [JsonProperty(PropertyName = "biddingEndDate")]
        public DateTime BiddingEndDate { get; set; }

        public List<Bidding> Biddings { get; set; }
    }
}
