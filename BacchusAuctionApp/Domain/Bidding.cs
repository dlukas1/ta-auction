﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Bidding
    {
        public string Id { get; set; }
        public Guid AuctionId { get; set; }
        public virtual Auction Auction { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public double Bet { get; set; }
        public DateTime BiddingTime { get; set; }
    }
}
