﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionEngine.Models
{
    public class Auction
    {
        public string AuctionId { get; set; }
        public string ProductId { get; set; }
        public virtual Product Product { get; set; }
        public enum Status { Finished = 0, Active = 1 }
        public string WinnerId { get; set; }
        public virtual User User { get; set; }
    }
}
