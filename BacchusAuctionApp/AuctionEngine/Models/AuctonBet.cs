﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionEngine.Models
{
    public class AuctonBet
    {
        public string Id { get; set; }
        public decimal BetSum { get; set; }
        public DateTime BetDate { get; set; }
        public string UserId { get; set; }
        public string AuctionId { get; set; }
        public virtual User User { get; set; }
        public virtual  Auction Auction { get; set; }
    }
}
