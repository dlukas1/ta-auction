﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BacchusAuctionApp.Migrations
{
    public partial class guid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Auctions",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(nullable: false),
                    BiddingEndDate = table.Column<DateTime>(nullable: false),
                    ProductCategory = table.Column<string>(nullable: true),
                    ProductDescription = table.Column<string>(nullable: true),
                    ProductName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auctions", x => x.ProductId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    RegistrationDateTime = table.Column<DateTime>(nullable: false),
                    Id = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => new { x.FirstName, x.LastName, x.RegistrationDateTime });
                });

            migrationBuilder.CreateTable(
                name: "Biddings",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AuctionId = table.Column<Guid>(nullable: false),
                    Bet = table.Column<double>(nullable: false),
                    BiddingTime = table.Column<DateTime>(nullable: false),
                    UserFirstName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    UserLastName = table.Column<string>(nullable: true),
                    UserRegistrationDateTime = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Biddings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Biddings_Auctions_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auctions",
                        principalColumn: "ProductId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Biddings_Users_UserFirstName_UserLastName_UserRegistrationDateTime",
                        columns: x => new { x.UserFirstName, x.UserLastName, x.UserRegistrationDateTime },
                        principalTable: "Users",
                        principalColumns: new[] { "FirstName", "LastName", "RegistrationDateTime" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Biddings_AuctionId",
                table: "Biddings",
                column: "AuctionId");

            migrationBuilder.CreateIndex(
                name: "IX_Biddings_UserFirstName_UserLastName_UserRegistrationDateTime",
                table: "Biddings",
                columns: new[] { "UserFirstName", "UserLastName", "UserRegistrationDateTime" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Biddings");

            migrationBuilder.DropTable(
                name: "Auctions");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
