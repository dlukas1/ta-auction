﻿var endBiddingTime = document.getElementsByClassName("timeLeft");
var timerLabel = document.getElementsByClassName("timer");
updateTimer();
setInterval(updateTimer, 1000);

function updateTimer() {
    
    for (var i = 0; i < endBiddingTime.length; i++) {
        // Get endBiddingDate
        var apiDate = new Date(endBiddingTime[i].innerHTML);
        // Fix hours to fit local time
        apiDate.setHours(apiDate.getHours() + 2);
        var countDownDate = apiDate.getTime();

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        var result = (days > 0 ? days + "d " : "")
            + (hours > 0 ? hours + "h " : "")
            + (minutes > 0 ? minutes + "m " : "")
            + (seconds + "s ");

        //Display result
        timerLabel[i].innerHTML = result;
        // If the countdown is over,mark "expired", hide button 
        // Reload index page
        // Or stay on betting page
        if (distance < 0) {
            document.getElementById("endDate").style.display = "none";
            timerLabel[i].innerHTML = "EXPIRED";
            if (location.pathname == '/') {
                location.reload();
            }
        }
    }
}