﻿var resetButton = document.getElementById("btn-reset");
createOptionBox();

function createOptionBox() {
    resetButton.style.display = "none";
    // Get select box
    var select = document.getElementById("filter");
    // Get all categories
    var allCategories = document.getElementsByClassName("categories");
    var array = [];
    // Fill array witn categories names to make them comparable
    for (var i = 0; i < allCategories.length; i++) {
        array[i] = allCategories[i].innerHTML;
    }
    // Create set of unique categories
    var uniqueCategories = Array.from(new Set(array));
    // Set unique categories as options in select box
    for (var i = 0; i < uniqueCategories.length; i++) {
        select.options[i+1] = new Option(uniqueCategories[i]);
    }
}

function filterElements() {
    resetButton.style.display = "block";
    var allCategories = document.getElementsByClassName("categories");
    var filterSelect = document.getElementById("filter");
    var selection = filterSelect.value;
    var rows = document.getElementsByClassName("auction");

    for (var i = 0; i < rows.length; i++) {
        if (selection != allCategories[i].innerHTML) {
           rows[i].style.display = "none";
        } 
    }
    filterSelect.style.display = "none";
}

// Reload Page
function reset() {
    location.reload();
}