﻿using System.Collections.Generic;
using BacchusAuctionApp.Domain;
using BacchusAuctionApp.Services;
using BacchusAuctionApp.ViewModels;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BacchusAuctionApp.Pages
{
    public class ResultModel : PageModel
    {

        public List<ResultViewModel> resultViewModels { get; set; }
        private readonly ApplicationDbContext _context;
        public ResultModel(ApplicationDbContext ctx)
        {
            _context = ctx;
        }
        public void OnGet()
        {
            BiddingService biddingService = new BiddingService(_context);
            resultViewModels = biddingService.GetBiddings();
        }
    }
}