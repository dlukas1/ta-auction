﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Domain;
using BacchusAuctionApp.Domain;
using ApiClient;
using BacchusAuctionApp.Services;

namespace BacchusAuctionApp.Pages.Auctions
{
    public class MakeBetModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        //Get List of Auctions
        List<Auction> Auctions = ApiResponse.GetAuctions();

        [BindProperty] public new User User { get; set; }

        [BindProperty] public Bidding Bidding { get; set; }

        [BindProperty] public Auction Auction { get; set; }

        public MakeBetModel(ApplicationDbContext ctx)
        {
            _context = ctx;
        }
        //On GET display selected item
        public IActionResult OnGet(Guid id)
        {

            Auction = Auctions.First(i => i.ProductId == id);
            if (Auction == null)
            {
                return NotFound();
            }
            return Page();
        }

        //On POST create new User and Bidding and save them and Product
        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var biddingTime = DateTime.Now;
                User.Id = User.FirstName + User.LastName + biddingTime;
                Bidding.UserId = User.Id;
                Bidding.AuctionId = Auction.ProductId;
                Bidding.BiddingTime = biddingTime;
                BiddingService biddingService = new BiddingService(_context);
                biddingService.SaveBidding(Auction, User, Bidding);
                return RedirectToPage("Index");
            }
            return Page();
        }

    }
}