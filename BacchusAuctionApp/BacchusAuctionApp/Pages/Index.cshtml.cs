﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Domain;
using ApiClient;


namespace BacchusAuctionApp.Pages.Auctions
{
    public class IndexModel : PageModel
    {
        public List<Auction> Auctions { get; set; }

        public void OnGet()
        {
            Auctions = ApiResponse.GetAuctions();
        }
    }
}