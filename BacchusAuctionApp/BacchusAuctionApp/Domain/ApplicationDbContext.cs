﻿using Microsoft.EntityFrameworkCore;
using Domain;

namespace BacchusAuctionApp.Domain
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Bidding> Biddings { get; set; }
        public DbSet<Auction> Auctions { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Create composite key for user ID
            modelBuilder.Entity<User>()
                .HasKey(u => new { u.FirstName, u.LastName, u.RegistrationDateTime });
        }
    }
}
