﻿using System;

namespace BacchusAuctionApp.ViewModels
{
    public class ResultViewModel
    {
        public DateTime BetTime { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public double BetSum { get; set; }
        public DateTime BiddingEndDate { get; set; }
    }
}
