﻿using System;
using BacchusAuctionApp.Domain;
using Domain;
using System.Collections.Generic;
using System.Linq;
using BacchusAuctionApp.ViewModels;

namespace BacchusAuctionApp.Services
{
    public class BiddingService
    {
        private readonly ApplicationDbContext _context;
        public BiddingService (ApplicationDbContext ctx)
        {
            _context = ctx;
        }

        public void SaveBidding(Auction auction, User user, Bidding bidding)
        {
            // DON't save same auction twice if it was already saved
            if (!_context.Auctions.Any(a => a.ProductId == auction.ProductId))
            {
                _context.Auctions.Add(auction);
            }
           
            _context.Users.Add(user);
            _context.Biddings.Add(bidding);
            _context.SaveChanges();
        }

        public List<ResultViewModel> GetBiddings()
        {
            var biddings =_context.Biddings.ToList();
            var products = _context.Auctions.ToList();

            var result = _context
                 .Biddings
                 .Where(x => x.Auction.BiddingEndDate.AddHours(2) < DateTime.Now)
                 .GroupBy(
                     a => a.Auction.ProductId,
                     bet => bet,
                     (id, bids) => new ResultViewModel
                     {
                         ProductId = id.ToString(),
                         BiddingEndDate = bids.OrderByDescending(a => a.Bet).First().Auction.BiddingEndDate,
                         BetSum = bids.OrderByDescending(a => a.Bet).First().Bet,
                         BetTime = bids.OrderByDescending(a => a.Bet).First().BiddingTime,
                         ProductName = bids.OrderByDescending(a => a.Bet).First().Auction.ProductName
                     }
                 ).OrderByDescending(x => x.BetTime);
            return result.ToList();              
        }
    }
}
