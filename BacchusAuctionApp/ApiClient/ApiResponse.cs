﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Domain;


namespace ApiClient
{
    public static class ApiResponse
    {
        public static List<Auction> GetAuctions()
        {
            IRequestHandler httpWebRequestHandler = new HttpWebRequestHandler();
            var response = GetAuctions(httpWebRequestHandler);
            return JsonConvert.DeserializeObject<List<Auction>>(response);
        }

        public static string GetAuctions(IRequestHandler requestHandler)
        {
            return requestHandler.GetAuctions(Constants.URL);
        }
    }
}
