﻿namespace ApiClient
{
    public interface IRequestHandler
    {
        string GetAuctions(string url);
    }
}
