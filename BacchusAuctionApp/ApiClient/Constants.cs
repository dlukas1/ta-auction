﻿//Constants for connecting API
namespace ApiClient
{
    public class Constants
    {
        public const string URL = "http://uptime-auction-api.azurewebsites.net/api/Auction";
        public const string UserAgent = "User-Agent";
        public const string UserAgentValue = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";

    }
}