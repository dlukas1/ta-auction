# Web Auction App

## Installation Guide
* Download project: git clone https://dlukas1@bitbucket.org/dlukas1/uptimeauction.git
* Open project in Visual Studio
* Go to Tools -> NuGet Package Manager -> Package Manager Console and type:
	- Add-Migration initial
 	- Update-Database
* Press Ctrl+F5 or Debug -> Start Without Debugging to run the app